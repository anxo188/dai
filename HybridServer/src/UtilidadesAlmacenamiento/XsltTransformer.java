package utilidadesalmacenamiento;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import customerrorhandler.SimpleErrorHandler;
import customexceptions.BadRequestException;
import customexceptions.InternalServerException;

public class XsltTransformer {
	
	public static String transformWithXSLT(String xml, String xslt, String xsd)
	throws BadRequestException, InternalServerException{
		try {
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer =
				tFactory.newTransformer(new StreamSource(new StringReader(xslt)));
			
			StringWriter writer = new StringWriter();
			Source xsdSource = new StreamSource(new StringReader(xsd));
			Schema schema =SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI).newSchema(xsdSource);
			Validator validator = schema.newValidator();
			validator.validate(new StreamSource(new StringReader(xml)));
			transformer.transform(
				new StreamSource(new StringReader(xml)), 
				new StreamResult(writer)
			);
			
			return writer.toString();
		
		}catch(TransformerException | SAXException e) {
			throw new BadRequestException();
			
		}catch(IOException e) {
			throw new InternalServerException();
		}
	}

}
