package utilidadesalmacenamiento;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import customexceptions.BadRequestException;
import customexceptions.InternalServerException;
import customexceptions.PageNotFoundException;

public class DBAbstractor implements Almacenamiento {
	private String db_url;
	private String db_user;
	private String db_password;

	public DBAbstractor(String db_url, String db_user, String db_password) {
		this.db_url = db_url;
		this.db_user = db_user;
		this.db_password = db_password;
		DriverManager.setLoginTimeout(30);
	}

	@Override
	public int existsResource(String uuid) {
		if (this.getResource(uuid) != null) {
			return 0;
		}
		return -1;

	}

	@Override
	public int insertNewResource(String key, String value) {
		try (final Connection dbConnection = this.getConnection();) {
			PreparedStatement statement = dbConnection.prepareStatement("INSERT INTO HTML (uuid,content) VALUES(?,?)");
			statement.setString(1, key);
			statement.setString(2, value);
			int result = statement.executeUpdate();
			if (result == 1) {
				return 0;
			} else {
				return -1;
			}
		} catch (SQLException e) {
			return -1;
		}
	}

	@Override
	public int insertNewResource(String key, String type, String value, String xsd) throws BadRequestException, InternalServerException {
		try (final Connection dbConnection = this.getConnection();) {
			PreparedStatement statement;
			switch (type) {
			case "html":
				statement = dbConnection.prepareStatement("INSERT INTO HTML (uuid,content) VALUES(?,?)");
				break;
			case "xml":
				statement = dbConnection.prepareStatement("INSERT INTO XML (uuid,content) VALUES(?,?)");
				break;
			case "xsd":
				statement = dbConnection.prepareStatement("INSERT INTO XSD (uuid,content) VALUES(?,?)");
				break;
			case "xslt":
				statement = dbConnection.prepareStatement("INSERT INTO XSLT (uuid,content,xsd) VALUES(?,?,?)");
				break;
			default:
				throw new BadRequestException();
			}
			
			statement.setString(1, key);
			statement.setString(2, value);
			if(xsd != null) {
				statement.setString(3, xsd);
			}
			int result = statement.executeUpdate();
			if (result == 1) {
				return 0;
			} else {
				return -1;
			}
		} catch (SQLException e) {
			throw new InternalServerException();
		}
	}

	private Connection getConnection() throws SQLException {
		try {
			return DriverManager.getConnection(this.db_url, this.db_user, this.db_password);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e.getMessage());
		}
	}

	@Override
	public int deleteResource(String uuid) throws PageNotFoundException, SQLException {

		try (final Connection dbConnection = this.getConnection();) {
			PreparedStatement statement = dbConnection.prepareStatement("DELETE FROM HTML WHERE uuid=?");
			statement.setString(1, uuid);
			int result = statement.executeUpdate();
			if (result == 1) {
				return 0;
			} else {
				throw new PageNotFoundException();
			}
		} catch (SQLException e) {
			throw new SQLException();
		}
	}
	
	@Override
	public int deleteResource(String uuid,String type) throws PageNotFoundException, SQLException, BadRequestException {

		try (final Connection dbConnection = this.getConnection();) {
			PreparedStatement statement;
			switch (type) {
			case "html":
				statement =  dbConnection.prepareStatement("DELETE FROM HTML WHERE uuid=?");
				break;
			case "xml":
				statement =  dbConnection.prepareStatement("DELETE FROM XML WHERE uuid=?");
				break;
			case "xsd":
				statement =  dbConnection.prepareStatement("DELETE FROM XSD WHERE uuid=?");
				break;
			case "xslt":
				statement =  dbConnection.prepareStatement("DELETE FROM XSLT WHERE uuid=?");
				break;
			default:
				throw new BadRequestException();
			}
			
			statement.setString(1, uuid);
			int result = statement.executeUpdate();
			if (result == 1) {
				return 0;
			} else {
				throw new PageNotFoundException();
			}
		} catch (SQLException e) {
			throw new SQLException();
		}
	}

	@Override
	public String getResource(String uuid) {
		try (final Connection dbConnection = this.getConnection();) {
			PreparedStatement statement = dbConnection.prepareStatement("SELECT content FROM HTML where uuid = ?");
			statement.setString(1, uuid);
			try (ResultSet result = statement.executeQuery()) {
				result.first();
				return result.getString(result.findColumn("content"));
			}
		} catch (SQLException e) {
			return null;
		}
	}
	
	@Override
	public String getResource(String uuid,String type) throws BadRequestException, PageNotFoundException,InternalServerException {
		try (final Connection dbConnection = this.getConnection();) {
			
			PreparedStatement statement;
			switch (type) {
			case "html":
				statement =  dbConnection.prepareStatement("SELECT content FROM HTML where uuid = ?");
				break;
			case "xml":
				statement =  dbConnection.prepareStatement("SELECT content FROM XML where uuid = ?");
				break;
			case "xsd":
				statement =  dbConnection.prepareStatement("SELECT content FROM XSD where uuid = ?");
				break;
			case "xslt":
				statement =  dbConnection.prepareStatement("SELECT content FROM XSLT where uuid = ?");
				break;
			default:
				throw new BadRequestException();
			}
			statement.setString(1, uuid);
			try (ResultSet result = statement.executeQuery()) {
				if(result.first()) {
					return result.getString(result.findColumn("content"));
				}else {
					throw new PageNotFoundException();
					
				}
			}
		} catch (SQLException e) {
			throw new InternalServerException();
		}
	}
	
	public String getAsociatedXsd(String uuid) throws PageNotFoundException, BadRequestException, InternalServerException{
		try (final Connection dbConnection = this.getConnection();) {
			PreparedStatement statement = dbConnection.prepareStatement("SELECT xsd FROM XSLT where uuid = ?");
			statement.setString(1, uuid);
			try (ResultSet result = statement.executeQuery()) {
				
				if(result.first() == false) {
					throw new PageNotFoundException();
				}
				String xsdUuid = result.getString(result.findColumn("xsd"));
				if(xsdUuid != null) {
					statement = dbConnection.prepareStatement("SELECT content FROM XSD where uuid = ?");
					statement.setString(1, xsdUuid);
					try (ResultSet resultXsd = statement.executeQuery()){
						resultXsd.first();
						String tmp = resultXsd.getString(resultXsd.findColumn("content"));
						if(tmp == null) {
							throw new PageNotFoundException();
						}else {
							return tmp;
						}
					}
				}else {
					throw new BadRequestException();
				}
			}
		} catch (SQLException e) {
			throw new InternalServerException();
		}
		
	}

	public String getUUIDsLinks() {
		StringBuilder result = new StringBuilder();

		try (final Connection dbConnection = this.getConnection();) {
			PreparedStatement statement = dbConnection.prepareStatement("SELECT uuid FROM HTML");
			result.append("<h2>HTML</h2>\n");
			try (ResultSet resultSet = statement.executeQuery()) {
				while (resultSet.next()) {
					String uuid = resultSet.getString(resultSet.findColumn("uuid"));
					String tmp = "<li><a href=\"/html?uuid=" + uuid + "\">" + uuid + "</a></li>\n";
					result.append(tmp);
				}
			}
			result.append("<h2>XML</h2>\n");
			statement = dbConnection.prepareStatement("SELECT uuid FROM XML");
			try (ResultSet resultSet = statement.executeQuery()) {
				while (resultSet.next()) {
					String uuid = resultSet.getString(resultSet.findColumn("uuid"));
					String tmp = "<li><a href=\"/xml?uuid=" + uuid + "\">" + uuid + "</a></li>\n";
					result.append(tmp);
				}
			}
			
			result.append("<h2>XSLT</h2>\n");
			statement = dbConnection.prepareStatement("SELECT uuid FROM XSLT");
			try (ResultSet resultSet = statement.executeQuery()) {
				while (resultSet.next()) {
					String uuid = resultSet.getString(resultSet.findColumn("uuid"));
					String tmp = "<li><a href=\"/xml?uuid=" + uuid + "\">" + uuid + "</a></li>\n";
					result.append(tmp);
				}
			}
			
			result.append("<h2>XSD</h2>\n");
			statement = dbConnection.prepareStatement("SELECT uuid FROM XSD");
			try (ResultSet resultSet = statement.executeQuery()) {
				while (resultSet.next()) {
					String uuid = resultSet.getString(resultSet.findColumn("uuid"));
					String tmp = "<li><a href=\"/xml?uuid=" + uuid + "\">" + uuid + "</a></li>\n";
					result.append(tmp);
				}
			}
		} catch (SQLException e) {
			return null;
		}
		return result.toString();
	}
	
	public List<String> getUUIDs(String type) throws BadRequestException,InternalServerException {
		
		try (final Connection dbConnection = this.getConnection();) {
			PreparedStatement statement;
		switch (type) {
		case "html":
			statement =  dbConnection.prepareStatement("SELECT uuid FROM HTML");
			break;
		case "xml":
			statement =  dbConnection.prepareStatement("SELECT uuid FROM XML");
			break;
		case "xsd":
			statement =  dbConnection.prepareStatement("SELECT uuid FROM XSD");
			break;
		case "xslt":
			statement =  dbConnection.prepareStatement("SELECT uuid FROM XSLT");
			break;
		default:
			throw new BadRequestException();
		}
		
			try (ResultSet resultSet = statement.executeQuery()) {
			
				List<String> toReturn = new LinkedList<String>();
				while (resultSet.next()) {
					String uuid = resultSet.getString(resultSet.findColumn("uuid"));
					toReturn.add(uuid);
				}
				return toReturn;
			} catch (SQLException e) {
				throw new InternalServerException();
			}
		} catch (SQLException e) {
			throw new InternalServerException();
		}	
	}
	

	public boolean closeConnection(Connection db) throws SQLException {
		db.close();
		return db.isClosed();
	}
}
