package utilidadesalmacenamiento;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import customexceptions.BadRequestException;
import customexceptions.InternalServerException;
import customexceptions.PageNotFoundException;

public class AlmacenMap implements Almacenamiento {

	private Map<String, String> storage;

	public AlmacenMap() {
		storage = new HashMap<String,String>();
		//this.insertNewResource("Welcome", value);
	}
	public AlmacenMap(Map<String, String> mapa) {
		storage = mapa;
	}

	@Override
	public int existsResource(String uuid) {
		// TODO Auto-generated method stub
		if(!storage.containsKey(uuid))
			return -1;
		return 0;
	}

	@Override
	public int insertNewResource(String key, String value) throws InternalServerException {
		try{
			storage.put(key, value);
		}catch(Exception e) {
			throw new InternalServerException();
		}
		return 0;
	}
	
	@Override
	public int insertNewResource(String key, String type, String value,String xsd) throws InternalServerException {
		try{
			storage.put(key, value);
		}catch(Exception e) {
			throw new InternalServerException();
		}
		return 0;
	}

	@Override
	public int deleteResource(String uuid) throws PageNotFoundException{
		if(storage.remove(uuid) == null) {
			throw new PageNotFoundException();
		}
		return 0;
	}
	
	@Override
	public int deleteResource(String uuid, String type)
			throws PageNotFoundException, SQLException, BadRequestException {
		if(storage.remove(uuid) == null) {
			throw new PageNotFoundException();
		}
		return 0;
	}

	@Override
	public String getResource(String uuid) throws PageNotFoundException {
		String result;
		if ((result = this.storage.get(uuid)) == null) {
			throw new PageNotFoundException("The requested uuid doesn't exists in the storage or has no value");
		} else
			return result;
	}
	
	@Override
	public String getResource(String uuid, String type) throws PageNotFoundException, BadRequestException {
		String result;
		if ((result = this.storage.get(uuid)) == null) {
			throw new PageNotFoundException("The requested uuid doesn't exists in the storage or has no value");
		} else
			return result;
		
	}
	
	public String getUUIDsLinks() {
		StringBuilder result = new StringBuilder();
		Iterator it = storage.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        String tmp = "<a href=\"/html?uuid="+pair.getKey()+"\">"+pair.getKey()+"</a>\n ";
	        result.append(tmp);
	    }
		return result.toString();
		
	}
	
	public String getAsociatedXsd(String uuid) throws PageNotFoundException,BadRequestException{
		return null;
	}
	@Override
	public List<String> getUUIDs(String type) throws BadRequestException, InternalServerException {
		// TODO Auto-generated method stub
		return null;
	}
	
	

}
