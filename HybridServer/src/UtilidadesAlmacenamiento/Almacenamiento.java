package utilidadesalmacenamiento;

import java.sql.SQLException;
import java.util.List;

import customexceptions.BadRequestException;
import customexceptions.InternalServerException;
import customexceptions.PageNotFoundException;

public interface Almacenamiento {
	
	int existsResource( String uuid);
	
	int insertNewResource(String key,String value) throws InternalServerException;
	
	int insertNewResource(String key,String type, String value,String xsd) throws InternalServerException,BadRequestException;
	
	int deleteResource( String uuid) throws PageNotFoundException, SQLException;
	
	int deleteResource( String uuid,String type) throws PageNotFoundException, SQLException,BadRequestException;
	
	String getResource(String uuid) throws PageNotFoundException;
	
	String getResource(String uuid,String tpye) throws PageNotFoundException,BadRequestException,InternalServerException;
	
	String getAsociatedXsd(String uuid) throws PageNotFoundException,BadRequestException,InternalServerException;
	
	String getUUIDsLinks();
	
	List<String> getUUIDs(String type) throws BadRequestException,InternalServerException;
	
}
