package customthreads;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.ws.Endpoint;

import es.uvigo.esei.dai.hybridserver.HybridServer;
import utilidadesalmacenamiento.Almacenamiento;
import webservices.OfferedServicesImplementation;

public class ServerThread implements Runnable {
	
	private Almacenamiento storage;
	private HybridServer server;
	private ExecutorService executorsPool;
	public ServerThread(Almacenamiento storage,HybridServer server,ExecutorService executorsPool) {
		this.storage = storage;
		this.server = server;
		this.executorsPool = executorsPool;
	}
	@Override
	public void run() {
		
		try (final ServerSocket serverSocket = new ServerSocket( server.getPort() ) ) {
			while (!server.getStop()) {
				Socket socket = serverSocket.accept();
				this.executorsPool.execute(new ServiceThread(socket, this.storage,this.server));
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}