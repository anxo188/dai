package customthreads;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import customexceptions.BadRequestException;
import customexceptions.InternalServerException;
import customexceptions.NotAllowedMethodException;
import customexceptions.PageNotFoundException;
import es.uvigo.esei.dai.hybridserver.HybridServer;
import es.uvigo.esei.dai.hybridserver.ServerConfiguration;
import es.uvigo.esei.dai.hybridserver.http.HTTPHeaders;
import es.uvigo.esei.dai.hybridserver.http.HTTPParseException;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;
import es.uvigo.esei.dai.hybridserver.http.MIME;
import utilidadesalmacenamiento.Almacenamiento;
import utilidadesalmacenamiento.XsltTransformer;
import webservices.OfferedServices;

public class ServiceThread implements Runnable {

	private Socket clientSocket = null;
	private Almacenamiento storage;
	private HybridServer server;

	public ServiceThread(final Socket clientSocket, Almacenamiento storage, HybridServer server) {
		this.storage = storage;
		this.clientSocket = clientSocket;
		this.server = server;
	}

	@Override
	public void run() {

		try (Socket clientSocket = this.clientSocket;
				InputStream input = clientSocket.getInputStream();
				OutputStream output = clientSocket.getOutputStream();) {
			HTTPResponse response = new HTTPResponse();
			if (this.server.getStop()) {
				response.setStatus(HTTPResponseStatus.valueOf("S200"));
				response.setVersion(HTTPHeaders.valueOf("HTTP_1_1").toString());
				String message = "Server is closing now";
				response.putParameter(HTTPHeaders.valueOf("CONTENT_LENGTH").toString(),
						Integer.toString(message.length()));
				response.setContent(message);
				output.write(response.toString().getBytes());
			} else {
				try {
					HTTPRequest request = new HTTPRequest(new InputStreamReader(input));
					String type = request.getResourceName();
					String uuid, data;
					switch (request.getMethod()) {
					case GET:
						uuid = request.getResourceParameters().get("uuid");
						String contentType;
						if(type.contentEquals("xml") || type.contentEquals("xsd") || type.contentEquals("xslt")) {
							contentType=MIME.APPLICATION_XML.getMime();
						}else {
							contentType=MIME.TEXT_HTML.getMime();
						}
						
						if (uuid == null) {
							uuid = server.getWelcomeUUID();
							String localLinks = this.storage.getUUIDsLinks();
							String externalLinks = this.getNetLinks();
							String listLinks;
							if (localLinks == null) {
								listLinks = "";
							} else {
								listLinks = "<p>Our hosted resources:</p>\n<ul>" + localLinks + "</ul>";
							}
							
							if(externalLinks != null) {
								listLinks = listLinks + externalLinks;
							}
							data = "<!DOCTYPE html>\n" + "<title>Hybrid Server</title>\n" + "<style>\n" + "* {\n"
									+ "	text-align: center;\n" + "	font-family: serif;\n" + "}\n" + "\n" + "h1, h2 {\n"
									+ "	font-family: sans-serif;\n" + "}\n" + "\n" + "ul {\n"
									+ "	list-style-type: \"\\25B6\";\n" + "}\n" + "\n" + "address {\n"
									+ "	display: inline-block;\n" + "	width: 30ch;\n" + "	overflow: hidden;\n"
									+ "	white-space: nowrap;\n" + "	text-overflow: ellipsis;\n" + "}\n" + "\n" + "a {\n"
									+ "	text-decoration: none;\n" + "	color: midnightblue;\n" + "}\n" + "</style>\n"
									+ "<h1>Hybrid Server</h1>\n" + "<h2>Localhost:</h2>\n"
									+ listLinks + "<p>Author:</p>\n" + "<ul>\n"
									+ "	<li><address><a href=\"mailto:jdcervinho@esei.uvigo.es\">Duran Cervino, Jose Angel</a></address></li>\n"
									+ "</ul>\n" + "";
						} else {
							
							try {
								data = storage.getResource(uuid,type);								
							}catch(PageNotFoundException e) {
								data = this.askToNet(uuid, type);
								if(data == null) {
									throw new PageNotFoundException();
								}
										
							}
							
							String xslt;
							if(type.contentEquals("xml") && (xslt = request.getResourceParameters().get("xslt")) !=null ) {
								String xsd = null;
								try {
									xsd = storage.getAsociatedXsd(xslt);									
								
								}catch(PageNotFoundException e) {
									xsd = this.askToNetAssociatedXSD(xslt);
									if(xsd == null) {
										throw new PageNotFoundException();
									}
								}
								try {
									xslt = storage.getResource(xslt,"xslt");									
								}catch(PageNotFoundException e) {
									xslt = this.askToNet(xslt, "xslt");
									if(xslt == null) {
										throw new PageNotFoundException();
									}
								}
								data = XsltTransformer.transformWithXSLT(data, xslt,xsd);
								contentType=MIME.TEXT_HTML.getMime();
							}
						}
						response.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), contentType);
						response.setStatus(HTTPResponseStatus.valueOf("S200"));
						response.setVersion(request.getHttpVersion());
						response.putParameter(HTTPHeaders.CONTENT_LENGTH.getHeader(),
								Integer.toString(data.length()));
						
						
						response.setContent(data);

						output.write(response.toString().getBytes());

						break;

					case POST:

						uuid = (UUID.randomUUID()).toString();
						String resource = request.getResourceParameter(type);
						
						if (resource == null) {
							throw new BadRequestException();
						}
						String xsd = null;
						if(type.contentEquals("xslt") && request.getResourceParameter("xsd") == null) {
							throw new BadRequestException();
						}else if(type.contentEquals("xslt")) {
							this.storage.getResource(request.getResourceParameter("xsd"), "xsd");
							xsd = request.getResourceParameter("xsd");
						}
						this.storage.insertNewResource(uuid, type,resource,xsd);
						data = ("<a href=\""+type+"?uuid=" + uuid + "\">" + uuid + "</a>");
						response.setStatus(HTTPResponseStatus.S200);
						response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
						response.putParameter(HTTPHeaders.CONTENT_LENGTH.getHeader(), Integer.toString(data.length()));
						response.setContent(data);
						if(type.contentEquals("html")) {
							response.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), "text/html");
						}else {
							response.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), "aplication/xml");
						}
						
						output.write(response.toString().getBytes());

						break;

					case DELETE:
						uuid = request.getResourceParameter("uuid");
						if (uuid == null) {
							throw new BadRequestException();
						}
						this.storage.deleteResource(uuid,type);
						response.setStatus(HTTPResponseStatus.S200);
						response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
						data = "The page has ben deleted succesfully";
						response.putParameter(HTTPHeaders.CONTENT_LENGTH.getHeader(), Integer.toString(data.length()));
						response.setContent(data);
						response.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), "text/html");
						output.write(response.toString().getBytes());

						break;

					default:
						throw new NotAllowedMethodException();
					}
				} catch (HTTPParseException e) {
						
					switch (e.getMessage()) {
					case "400":
						response.setStatus(HTTPResponseStatus.S400);
						break;
					case "505":
						response.setStatus(HTTPResponseStatus.S505);
						break;
					case "500":
					default:
						response.setStatus(HTTPResponseStatus.S500);
						break;
					}
					response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
					output.write(response.toString().getBytes());

				} catch (PageNotFoundException e) {
					response.setStatus(HTTPResponseStatus.S404);
					response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
					output.write(response.toString().getBytes());
				} catch (BadRequestException e) {
					response.setStatus(HTTPResponseStatus.S400);
					response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
					output.write(response.toString().getBytes());
				} catch(InternalServerException | SQLException e) {
					response.setStatus(HTTPResponseStatus.S500);
					response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
					output.write(response.toString().getBytes());
				} catch (Exception e) {
					response.setStatus(HTTPResponseStatus.S500);
					response.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
					output.write(response.toString().getBytes());
				}

			}
		} catch (IOException e) {
			System.out.println("ExcepcionIO: " + e.getMessage());
		}
	}
	
	private String askToNet(String uuid,String type) {
		
		List<OfferedServices> servicesList = this.server.getConnectedServices();
		if(servicesList == null || servicesList.isEmpty()) {
			return null;
		}
		
		Iterator<OfferedServices> iterator = servicesList.iterator();
		String found;
		while(iterator.hasNext()) {
			OfferedServices service = iterator.next();
			if(service != null) {
				switch(type) {
				case "html":
					found = service.getHTML(uuid);
					break;
				case "xml":
					found = service.getXML(uuid);
					break;
				case "xsd":
					found = service.getXSD(uuid);
					break;
				case "xslt":
					found = service.getXSLT(uuid);	
					break;
				default:
					found = null;
					break;
				}
				if(found != null) {
					return found;
				}				
			}
		}
		return null;
		
	}
	
	private String askToNetAssociatedXSD(String uuid) {
		List<OfferedServices> servicesList = this.server.getConnectedServices();
		if(servicesList == null || servicesList.isEmpty()) {
			return null;
		}
		Iterator<OfferedServices> iterator = servicesList.iterator();
		String found = null;
		while(iterator.hasNext()) {
			
			OfferedServices service = iterator.next();
			if(service != null) {
				found = service.getAssociatedXSD(uuid);
				if(found != null) {
					return found;
				}
			}
		}
		return null;
	}
	
	private String getNetLinks() {
		
		List<OfferedServices> servicesList = this.server.getConnectedServices();
		if(servicesList == null || servicesList.isEmpty()) {
			return null;
		}
		
		Iterator<OfferedServices> iterator = servicesList.iterator();

		List<ServerConfiguration> servers = this.server.getConnectedServers();
		Iterator<ServerConfiguration> serversIterator = servers.iterator();
		
		StringBuilder result = new StringBuilder();
		
		
		while(iterator.hasNext()) {
			OfferedServices service = iterator.next();
			ServerConfiguration server = serversIterator.next();
			
			if(service != null) {
					
				result.append("<h1>"+ server.getName() +"</h1>\n");
				
				
				String[] htmls = service.getHTMLlist();
				result.append("<h2>HTML</h2>\n<ul>\n");
				for(int i=0; i< htmls.length;i++){
					String tmp = "<li><a href=\"/html?uuid=" + htmls[i] + "\">" + htmls[i] + "</a></li>\n";
					result.append(tmp);				
				}
				
				
				String[] xmls = service.getXMLlist();
				
				result.append("</ul>\n<h2>XML</h2>\n<ul>\n");
				for(int i=0; i< xmls.length;i++){
					String tmp = "<li><a href=\"/html?uuid=" + xmls[i] + "\">" + xmls[i] + "</a></li>\n";
					result.append(tmp);				
				}
				
				
				String[] xsds = service.getXSDlist();
				result.append("</ul>\n<h2>XSD</h2>\n<ul>\n");
				for(int i=0; i< xsds.length;i++){
					String tmp = "<li><a href=\"/html?uuid=" + xsds[i] + "\">" + xsds[i] + "</a></li>\n";
					result.append(tmp);				
				}
				
				String[] xslts = service.getXSLTlist();
				result.append("</ul>\n<h2>XSLT</h2>\n<ul>\n");
				for(int i=0; i< xslts.length;i++){
					String tmp = "<li><a href=\"/html?uuid=" + xslts[i] + "\">" + xslts[i] + "</a></li>\n";
					result.append(tmp);				
				}
				result.append("</ul>");

			}
		}
		
		return result.toString();
	}

}
