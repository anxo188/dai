package es.uvigo.esei.dai.hybridserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Properties;

public class Launcher {

	
	
	

	public static void main(String[] args) {
		//Defaults
		final int numClients=50 ;
		final int port=8888;
		final String db_url="jdbc:mysql://localhost:3306/hstestdb";
		final String db_user="hsdb";
		final String db_password="hsdbpass";
		
		Properties configuration = new Properties();
		HybridServer server = null;
		if(args.length == 0) {
			server = new HybridServer();
				
		}else if(args.length > 1){
			System.out.println("N�mero de par�metros pasados excedido\n"
					+ "Solo se admite como m�ximo un par�metro que puede ser:\n"
					+ "'-h' muestra la ayuda del servidor\n"
					+ "'-defaults' muestra la configuraci�n por defecto del servidor\n"
					+ "ningun par�metro ejecuta el servidor en su configuraci�n por defecto\n"
					+ "un fichero con la configuraci�n del servidor ejecuta el servidor con la configuraci�n proporcionada");
			
		}else if(args[0].contentEquals("-h")) {
			System.out.println("-------------------------------");
			System.out.println("----- Ayuda del servidor  -----");
			System.out.println("-------------------------------");
			System.out.println("Este programa sirve como lanzador del servidor de p�ginas web"
						+ " para el proyecto de DAI, su funcionamiento es el siguiente:");
			System.out.println("Para ejecutar el programa introduzca como par�metro el"
							+ " fichero con las configuraciones adecuadas,");
			System.out.println("en otro caso"
							+ " no introduzca par�metros para utilizar la configuraci�n por defecto,"
							+ "puede usar '-defaults' para ver las opciones por defecto");
			System.out.println("En caso de querer usar un fichero de configuraciones se debe seguir el siguiente formato: ");
				System.out.println("port=<Value>");
				System.out.println("numClients=<Value>");
				System.out.println("db.url=<Value>");
				System.out.println("db.user=<Value>");
				System.out.println("db.password=<Value>");
				
		}else if(args[0].contentEquals("-defaults")) {
			System.out.println("-------------------------------");
			System.out.println("----- Ayuda del servidor  -----");
			System.out.println("-------------------------------");			
			System.out.println("Se mostrar�n los par�metros por defecto: ");
			System.out.println("-------------------------------");	
			System.out.println("port: "+Integer.toString(port));
			System.out.println("numClients: "+Integer.toString(numClients));
			System.out.println("db.url: "+db_url);
			System.out.println("db.user: "+db_user);
			System.out.println("db.password: "+db_password);
			
		}else{
			System.out.println("Bienvenido al lanzador del servidor HTML DAI");
			System.out.println("Iniciando cargado de configuraciones");
			/* Código para el uso de properties
			 * try(Reader cargadorConfiguracion = new InputStreamReader(new FileInputStream(args[0]) ) ){
				configuration = cargaValidaConfig(cargadorConfiguracion);
				server = new HybridServer(configuration);
			*/
			try {
				server = new HybridServer( cargadorConfig(args[0]) );
				
			}catch(FileNotFoundException exception) {
				System.out.println("El fichero pasado no pudo ser recuperado");
			}catch(IOException exception) {
				System.out.println("Error inesperado: " + exception.getMessage());
			}catch(NumberFormatException exception) {
				System.out.println("Configuraci�n incorrecta detectada: "+exception.getMessage());
			}catch(Exception e) {
				System.out.println("Abortado inicio del servidor");
			}
		}
		if(server != null) {
			System.out.println("Iniciando servidor...");
			server.start();
			System.out.println("Servidor iniciado");
			//System.out.println("Los logs del sistema se almacenar�n en: ");
			System.out.println("Presiona Enter para parar el servidor.");
			try {
				System.in.read();
			} catch (final IOException exc) {
				System.out.println("I/O error while reading from standard input.");
			}

			server.stop();
			System.out.println("Servidor detenido");
		}else {
			System.out.println("Abortado inicio del servidor");
		}
	}
	
	private static Configuration cargadorConfig(String path) throws Exception{
		return XMLConfigurationLoader.load(new File(path));
	}
	
	
	private static Properties cargaValidaConfig(Reader cargadorConfig) throws IOException {
		Properties result = new Properties();
		result.load(cargadorConfig);
		
		try{
			Integer.valueOf(result.getProperty("port"));
		}catch(NumberFormatException e) {
			throw new NumberFormatException("Propiedad port incorrecta no puede interpretarse como INT");
		}
		System.out.println("\tCargado puerto: "+result.getProperty("port"));
		try{
			Integer.valueOf(result.getProperty("numClients"));
		}catch(NumberFormatException e) {
			throw new NumberFormatException("Propiedad numClients incorrecta no puede interpretarse como INT");
		}
		System.out.println("\tCargado numClients:"+result.getProperty("numClients"));
		System.out.println("\tCargado db.user:"+result.getProperty("db.user"));
		System.out.println("\tCargado db.url:"+result.getProperty("db.url"));
		System.out.println("\tCargado db.password:"+result.getProperty("db.password"));
		return result;
	}
	
}
