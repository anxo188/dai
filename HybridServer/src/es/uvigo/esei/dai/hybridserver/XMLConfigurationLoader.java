/**
 *  HybridServer
 *  Copyright (C) 2017 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.esei.dai.hybridserver;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import customerrorhandler.SimpleErrorHandler;

public class XMLConfigurationLoader {
	

	public static Configuration load(File xmlFile) throws ParserConfigurationException, SAXException, IOException {
		// ConstrucciÃ³n del parser del documento activando validaciÃ³n
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setValidating(true);
		factory.setNamespaceAware(true);
		factory.setAttribute(
			"http://java.sun.com/xml/jaxp/properties/schemaLanguage", 
			XMLConstants.W3C_XML_SCHEMA_NS_URI
		);
		
		// Al construir el parser hay que aÃ±adir un manejador de errores
		DocumentBuilder builder = factory.newDocumentBuilder();
		builder.setErrorHandler(new SimpleErrorHandler());
		
		// Parsing y validaciÃ³n del documento
		 Document doc = builder.parse(xmlFile);
		 
		 
		 
		 Configuration confBuilder = new Configuration();

		 //Configuracion de connections
		 Node http = doc.getElementsByTagName("http").item(0).getFirstChild();
		 Node webService = doc.getElementsByTagName("webservice").item(0).getFirstChild();
		 Node numClients = doc.getElementsByTagName("numClients").item(0).getFirstChild();
		
		 if(http == null || webService == null || numClients == null) {
			 throw new ParserConfigurationException();
		 }
			
		 confBuilder.setHttpPort(Integer.parseInt(http.getNodeValue()) );
		 confBuilder.setNumClients(Integer.parseInt(numClients.getNodeValue()));
		 confBuilder.setWebServiceURL(webService.getNodeValue());
			
		 //Configuracion de DB
		 Node db_user = doc.getElementsByTagName("user").item(0).getFirstChild();
		 Node db_password = doc.getElementsByTagName("password").item(0).getFirstChild();
		 Node db_url = doc.getElementsByTagName("url").item(0).getFirstChild();
		 
		 if(db_user == null || db_password == null || db_url == null) {
			 throw new ParserConfigurationException();
		 }
			
		 confBuilder.setDbUser(db_user.getNodeValue());
		 confBuilder.setDbPassword(db_password.getNodeValue());
		 confBuilder.setDbURL(db_url.getNodeValue());
		 
		 //Configuracion de servidores
		 Node serversNode = doc.getElementsByTagName("servers").item(0);
		 NodeList serversList = serversNode.getChildNodes();
		 Node server;
		 List<ServerConfiguration> servers = new LinkedList<ServerConfiguration>();
		 int index = 0;
		 while( (server = serversList.item(index)) != null ) {
			 if(server.getLocalName() == "server") {
				 NamedNodeMap attrs = server.getAttributes();
				 if(attrs == null) {
					 throw new ParserConfigurationException();
				 }
				 
				 String name = attrs.getNamedItem("name").getNodeValue();
				 String wdsl = attrs.getNamedItem("wsdl").getNodeValue();
				 String namespace = attrs.getNamedItem("namespace").getNodeValue();
				 String service = attrs.getNamedItem("service").getNodeValue();
				 String httpAddress = attrs.getNamedItem("httpAddress").getNodeValue();
				 ServerConfiguration serverConf = new ServerConfiguration(
						 name,wdsl,namespace,service,httpAddress
						 );
				 servers.add(serverConf);				 
			 }
			 index ++;
		 }
		confBuilder.setServers(servers);
		
		 return confBuilder;
	}	
}