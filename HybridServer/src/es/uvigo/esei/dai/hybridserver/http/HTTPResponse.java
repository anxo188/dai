package es.uvigo.esei.dai.hybridserver.http;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HTTPResponse {
	private HTTPResponseStatus status;
	private String version;
	private String content;
	private final Map<String, String> parameters = new HashMap<>();
	
	public HTTPResponse() {
	}

	public HTTPResponseStatus getStatus() {
		// TODO Auto-generated method stub
		return this.status;
	}

	public void setStatus(HTTPResponseStatus status) {
		this.status = status;
	}

	public String getVersion() {
		// TODO Auto-generated method stub
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getContent() {
		// TODO Auto-generated method stub
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Map<String, String> getParameters() {
		// TODO Auto-generated method stub
		return this.parameters;
	}

	public String putParameter(String name, String value) {
		// TODO Auto-generated method stub
		return this.parameters.put(name,value);
	}

	public boolean containsParameter(String name) {
		// TODO Auto-generated method stub
		
		return this.parameters.containsKey(name);
	}

	public String removeParameter(String name) {
		// TODO Auto-generated method stub
		return this.parameters.remove(name);
	}

	public void clearParameters() {
		this.parameters.clear();
	}

	public List<String> listParameters() {
		// TODO Auto-generated method stub
		return null;
	}

	public void print(Writer writer) throws IOException {
		if (status == null || version == null) {
			throw new IllegalStateException("Can't print a HTTP response without status code and version");
		}

		writer.write(version + " " + status.getCode() + " " + status.getStatus() + "\r\n");

		// Write headers
		int explicitContentLength = -1;
		for (final Map.Entry<String, String> parameterPair : parameters.entrySet()) {
			final String key = parameterPair.getKey();
			String value = parameterPair.getValue();

			// Check if we're about to send the content length header.
			// We want to make sure it's correct
			if (key.equalsIgnoreCase(HTTPHeaders.CONTENT_LENGTH.getHeader())) {
				try {
					explicitContentLength = Integer.parseUnsignedInt(parameterPair.getValue());
				} catch (final NumberFormatException exc) {
					// Ignore, we want explicitContentLength to keep its initial value
				}

				if ((content == null && explicitContentLength != 0) ||
					(content != null && explicitContentLength != content.length())
				) {
					// The content length header is not valid, so just replace it silently
					value = Integer.toString(explicitContentLength);
				}
			}

			writer.write(key + ": ");
			writer.write(value);
			writer.write("\r\n");
		}

		// If there is a non-empty message body, but we didn't receive
		// any content length header, add it
		if (content != null && !content.isEmpty() && explicitContentLength < 0) {
			writer.write(HTTPHeaders.CONTENT_LENGTH.getHeader() + ": " + content.length() + "\r\n");
		}

		// Write header ending
		writer.write("\r\n");

		// Write content, if any
		if (content != null && !content.isEmpty()) {
			writer.write(content);
		}	
	}

	@Override
	public String toString() {
		final StringWriter writer = new StringWriter();

		try {
			this.print(writer);
		} catch (IOException e) {
		}

		return writer.toString();
	}
}
