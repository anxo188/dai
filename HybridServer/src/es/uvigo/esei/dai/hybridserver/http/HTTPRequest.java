package es.uvigo.esei.dai.hybridserver.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class HTTPRequest {

	/*
	 * Method of the request, allowed values are GET,DELETE,POST,HEAD
	 */
	private HTTPRequestMethod method;
	/*
	 * The http's protocol version in form of HTTP/x.x
	 */
	private String httpVersion;
	/*
	 * The uri string with the parameters
	 * /example/uri/index.php?name=value&name2=value2
	 */
	private String resourceChain;
	/*
	 * The uri string without parameters and initial slash example/uri/index.php
	 */
	private String resourceName;
	/*
	 * The uri separated into an array [example,uri,index.php]
	 */
	private String[] resourcePath;

	/*
	 * The content send in the request
	 */
	private String content;
	
	private final Pattern uriRegexp = Pattern.compile(
			"^/(?:[A-za-z0-9_.!~*'():@&=+$,-]|%[0-9a-fA-F]{2})*(?:;(?:[A-za-z0-9_.!~*'():@&=+$,-]|%[0-9a-fA-F]{2})*)*(?:/(?:[A-za-z0-9_.!~*'"+
			"():@&=+$,-]|%[0-9a-fA-F]{2})*(?:;(?:[A-za-z0-9_.!~*'():@&=+$,-]|%[0-9a-fA-F]{2})*)*)*(?:\\?(?:[;/?:@&=+$,a-zA-Z0-9_.!~*'()-]|%"+
					"[0-9a-fA-F]{2})*)?$");
	
	private boolean encoded = false;
	// Uses LinkedHashMap in order to maintain order
	private Map<String, String> parameters = new LinkedHashMap<String, String>();
	private Map<String, String> headerParameters = new LinkedHashMap<String, String>();

	public HTTPRequest(Reader reader) throws IOException, HTTPParseException {

		/**
		 * Buffers the input in order to be able to read a line in one operation
		 */
		BufferedReader requestReader = new BufferedReader(reader);

		/*
		 * Request = Request-Line ; Section 5.1 *(( general-header ; Section 4.5 |
		 * request-header ; Section 5.3 | entity-header ) CRLF) ; Section 7.1 CRLF
		 * [message-body ] ; Section 4.3
		 */

		/*
		 * The Request-Line begins with a method token, followed by the Request-URI and
		 * the protocol version, and ending with CRLF. The elements are separated by SP
		 * characters. No CR or LF is allowed except in the final CRLF sequence.
		 * Request-Line = Method SP Request-URI SP HTTP-Version CRLF Example: GET
		 * /resource/path/index.php HTTP/1.1\r\n Request parameters are a pair
		 * name:value
		 */

		String bufferLine = requestReader.readLine();
		if (bufferLine == null) {
			throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S400.getCode()));
		}

		// Obtains the parts of the request line (method requested-URI HTTP-Version)
		String[] separatedFields = bufferLine.split(" ");

		// Launch exception whenever any of those fields doesn't exists
		if (separatedFields.length != 3) {
			throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S400.getCode()));
		}

		String requestedMethod = separatedFields[0];
		this.method = HTTPRequestMethod.valueOf(requestedMethod);
		this.httpVersion = separatedFields[2];
		if (!httpVersion.matches("^HTTP\\/1\\.[0-9]+$")) {
			throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S505.getCode()));
		}

		this.resourceChain = separatedFields[1];

		// As there are parameters in the uri it is splited
		String[] parts = resourceChain.split("\\?", 2);

		// If the uri is like "/" the resource is an empty string and the path also
		if (parts[0].contentEquals("/")) {

			this.resourceName = new String();
			this.resourcePath = new String[0];

		} else {// Extracts the initial "/" because is unnecessary

			parts[0] = parts[0].substring(1, parts[0].length());
			String[] tmp = parts[0].split("\\/");// Splits rout/example/mm -> [rout,example,mm]

			this.resourceName = parts[0];// Resource string version html,xml,etc...
			this.resourcePath = tmp; // Resource array version

		}

		switch (requestedMethod) {
		case "GET":
		case "DELETE":
		case "HEAD":
			if(!this.uriRegexp.matcher(this.resourceChain).matches()) {
				throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S400.getCode()));
			}
			
			if(parts.length > 1) {
				String[] allParameters = parts[1].split("&");// Split uri parameters to pairs name=value

				for (int i = 0; i < allParameters.length; i++) {
						
					String[] parameters = allParameters[i].split("=");
					if(parameters.length != 2) {
						throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S400.getCode()));
					}
					this.parameters.put(parameters[0], parameters[1]);
				}
			}
			
			break;
		default:
			break;
		}

		this.parseHeaderParameters(requestReader);
		
		if(requestedMethod.equals("POST")) {
			String contentType;
			if( ( contentType = this.headerParameters.get(HTTPHeaders.CONTENT_TYPE.getHeader()) ) != null && contentType.startsWith("application/x-www-form-urlencoded") )  {
				this.encoded = true;
			}try {
				saveContent(requestReader);
	
				this.parseBodyParameters(this.content);
				
			}catch(IOException e) {
				throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S500.getCode()));
			}
		}

	}

	private void parseHeaderParameters(BufferedReader input) throws HTTPParseException, IOException {

		/*
		 * According to RFC 2616 section 4.2 the format of headers parameters are:
		 * <Header> : <Value>
		 * CRLF
		 * CRLF
		 */

		boolean blankLine = false;
		String line;
		while (!blankLine && (line = input.readLine()) != null) {
			if (line.isEmpty()) {
				blankLine = true;
			} else {

				String[] tmpMap = line.split(":", 2);

				if (tmpMap.length != 2) {

					throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S400.getCode()));

				} else {

					this.headerParameters.put(tmpMap[0], tmpMap[1].replaceAll("\\s+", ""));
				}
			}
		}

		if (!blankLine) {
			throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S400.getCode()));
		}
	}
	
	private void parseBodyParameters(String bodyContent) throws HTTPParseException{
		
		String[] allParameters = bodyContent.split("&");// Split uri parameters to pairs name=value

		for (int i = 0; i < allParameters.length; i++) {

			String[] parameter = allParameters[i].split("=", 2);
			
			if (parameter.length != 2) {
				throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S400.getCode()));
			}

			if( this.encoded )  {
				try {
					this.parameters.put(URLDecoder.decode(parameter[0], "UTF-8"), URLDecoder.decode(parameter[1], "UTF-8"));
					
				}catch(UnsupportedEncodingException exception) {
					throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S400.getCode()));
				}
			}else {
				this.parameters.put(parameter[0], parameter[1]);
			}
		}
	}
	
	
	
	

	private void saveContent(BufferedReader reader) throws HTTPParseException,IOException  {
			
		
		StringBuilder tmpRawContent = new StringBuilder();
		int chunkLength = 2 * 1024 * 1024;
		char[] buffer = new char[chunkLength];
		int filledChunks = this.getContentLength() / chunkLength;
		//Filter to the min value in case of g
		int remainingChars = Math.min( this.getContentLength() % chunkLength, Integer.MAX_VALUE);
		
		try {
			//Read Packages of characters in amounts of 2MiB to be kind with the RAM
			for (int readedChunks = 0; readedChunks < filledChunks; readedChunks++) {
				if(reader.read(buffer) != chunkLength) {
					throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S400.getCode()));
				}
				tmpRawContent.append(buffer);
			}
			if(remainingChars > 0) {
				if(reader.read(buffer,0,remainingChars) != remainingChars) {
					throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S400.getCode()));
				}
				tmpRawContent.append(buffer,0,remainingChars);
				
			}
			this.content = tmpRawContent.toString();
			
		}catch(UnsupportedEncodingException exception) {
			throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S400.getCode()));
		} catch (IOException exception) {
			throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S400.getCode()));
		}
			
		if( this.encoded )  {

			try {
				this.content = URLDecoder.decode(this.content, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new HTTPParseException(Integer.toString(HTTPResponseStatus.S500.getCode()));
			}		
		
		}
		
	}

	
	
	

	public HTTPRequestMethod getMethod() {
		return this.method;
	}

	public String getResourceChain() {

		return this.resourceChain;
	}

	public String[] getResourcePath() {
		// TODO Auto-generated method stub
		return this.resourcePath;
	}

	public String getResourceName() {
		// TODO Auto-generated method stub
		return this.resourceName;
	}

	public Map<String, String> getResourceParameters() {

		return this.parameters;
	}

	public String getHttpVersion() {
		return this.httpVersion;
	}

	public Map<String, String> getHeaderParameters() {
		return headerParameters;
	}
	
	public String getResourceParameter(String key) {
		return this.parameters.get(key);
	}

	public String getContent() {
		// TODO Auto-generated method stub
		return this.content;
	}

	public int getContentLength() {
		if (headerParameters.containsKey(HTTPHeaders.CONTENT_LENGTH.getHeader())) {
			return Integer.parseInt(headerParameters.get(HTTPHeaders.CONTENT_LENGTH.getHeader()));
		} else {
			return 0;
		}

	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(this.getMethod().name()).append(' ').append(this.getResourceChain())
				.append(' ').append(this.getHttpVersion()).append("\r\n");

		for (Map.Entry<String, String> param : this.getHeaderParameters().entrySet()) {
			sb.append(param.getKey()).append(": ").append(param.getValue()).append("\r\n");
		}

		if (this.getContentLength() > 0) {
			sb.append("\r\n").append(this.getContent());
		}

		return sb.toString();
	}
}
