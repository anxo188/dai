package es.uvigo.esei.dai.hybridserver;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.ws.Endpoint;

import customexceptions.InternalServerException;
import customexceptions.PageNotFoundException;
import customthreads.ServerThread;
import es.uvigo.esei.dai.hybridserver.http.HTTPParseException;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import utilidadesalmacenamiento.AlmacenMap;
import utilidadesalmacenamiento.Almacenamiento;
import utilidadesalmacenamiento.DBAbstractor;
import webservices.OfferedServices;
import webservices.OfferedServicesImplementation;
import webservices.WebServiceClient;

public class HybridServer {
	private static final int SERVICE_PORT = 8888;
	private Thread serverThread;
	private boolean stop;
	private Properties properties;
	private Almacenamiento storage;
	private String welcomeUUID;
	private final int numClients=50 ;
	private final int port=8888;
	private final String db_url="jdbc:mysql://localhost:3306/hstestdb";
	private final String db_user="hsdb";
	private final String db_password="hsdbpass"; 
	private Configuration config = new Configuration();
	private Endpoint webServiceEndpoint;
	private List<OfferedServices> serversServices;
	private ExecutorService executorsPool;
	public HybridServer() {

		this.properties = new Properties();
		properties.setProperty("port", Integer.toString(port));
		properties.setProperty("numClients", Integer.toString(numClients));
		properties.setProperty("db.url", db_url);
		properties.setProperty("db.user", db_user);
		properties.setProperty("db.password", db_password);
		
		this.storage = new DBAbstractor(
				properties.getProperty("db.url").toString(),
				properties.getProperty("db.user").toString(),
				properties.getProperty("db.password").toString()
					);
		this.executorsPool = Executors.newFixedThreadPool(this.getNumClients());
	}

	public HybridServer(Map<String, String> pages) {

		this.properties = new Properties();
		properties.setProperty("port", Integer.toString(port));
		properties.setProperty("numClients", Integer.toString(numClients));
		properties.setProperty("db.url", db_url);
		properties.setProperty("db.user", db_user);
		properties.setProperty("db.password", db_password);

		this.storage = new AlmacenMap(pages);
		this.executorsPool = Executors.newFixedThreadPool(this.getNumClients());
	}

	public HybridServer(Properties properties) {
		
		if (properties == null) {
			throw new IllegalArgumentException("Introduced invalid properties");
		}
		
		this.config.setHttpPort( Integer.parseInt(properties.getProperty("port")));
		this.config.setNumClients( Integer.parseInt(properties.getProperty("numClients")) );
		this.config.setDbURL(properties.getProperty("db.url").toString());
		this.config.setDbUser(properties.getProperty("db.user").toString());
		this.config.setDbPassword(properties.getProperty("db.password").toString());
		
		this.storage = new DBAbstractor(
				properties.getProperty("db.url").toString(),
				properties.getProperty("db.user").toString(),
				properties.getProperty("db.password").toString()
					);
		this.executorsPool = Executors.newFixedThreadPool(this.getNumClients());

	}
	
	public HybridServer(Configuration conf) throws InternalServerException {
			this.config=conf;
			
			this.storage=new DBAbstractor( 
					config.getDbURL(),
					config.getDbUser(),
					config.getDbPassword()
					);
			this.executorsPool = Executors.newFixedThreadPool(this.getNumClients());	
			String webService = this.getWebServices();
			if(webService != null) {
				this.webServiceEndpoint = Endpoint.publish(webService,new OfferedServicesImplementation(storage));
				this.webServiceEndpoint.setExecutor(this.executorsPool);
			}
	}

	

	public void start() {
		
		this.stop = false;
		this.welcomeUUID = UUID.randomUUID().toString();			
		this.serverThread = new Thread(new ServerThread(this.storage,this,this.executorsPool));
		this.serverThread.start();
	}

	public void stop() {
		this.stop = true;
		//try (Socket socket = new Socket("localhost", Integer.parseInt(this.properties.getProperty("port") ) ) )  {
		try (Socket socket = new Socket("localhost",this.config.getHttpPort() ) )  {	
		// Esta conexión se hace, simplemente, para "despertar" el hilo servidor
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		try {
			
			if(this.webServiceEndpoint!=null) {
				this.webServiceEndpoint.stop();				
			}
			
			this.serverThread.join();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		this.serverThread = null;
	}

	public int getNumClients() {
		//return Integer.parseInt(this.properties.getProperty("numClients"));
		int aux = this.config.getNumClients();
		return aux;
	}

	public int getPort() {
		//String value = this.properties.getProperty("port");
		//return Integer.parseInt(value);
		return this.config.getHttpPort();
	}
	
	public boolean getStop() {
		return this.stop;
	}
	
	public String getWelcomeUUID() {
		return this.welcomeUUID;
	}

	public String getWebServices() {
		return this.config.getWebServiceURL();
	}
	
	public List<ServerConfiguration> getConnectedServers(){
		return this.config.getServers();
	}
	
	public List<OfferedServices> getConnectedServices(){
		List<ServerConfiguration> servers = this.getConnectedServers();
		if (servers != null && !servers.isEmpty()) {
			List<OfferedServices> servicesList = new LinkedList<OfferedServices>();
			Iterator<ServerConfiguration> itServer = servers.iterator();
			while (itServer.hasNext()) {
				WebServiceClient client;
				try {
					client = new WebServiceClient(itServer.next());
					servicesList.add(client.getServices());
				} catch (MalformedURLException e) {
					e.printStackTrace();
				}
			}
			return servicesList;
		}
		return null;
	}

}
