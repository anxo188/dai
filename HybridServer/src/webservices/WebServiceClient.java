package webservices;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceException;

import es.uvigo.esei.dai.hybridserver.ServerConfiguration;

public class WebServiceClient {
	private URL url;
	private QName name;
	private Service service;
	private OfferedServices services;
	
	
	public WebServiceClient(ServerConfiguration server) throws MalformedURLException {
		try {
			this.url = new URL(server.getWsdl());
			this.name = new QName(server.getNamespace(),server.getService());
			this.service = Service.create(this.url,this.name);
			this.services = service.getPort(OfferedServices.class);
		}catch(WebServiceException e) {
			System.out.println("Error al conectarse a: "+this.name+"\n"+
					"Del servidor: "+server.getName()+"\n"+
					"Error: "+e.getLocalizedMessage()+"\n\n\n");
		}
	}
	
	public OfferedServices getServices() {
		return this.services;
	}
	

}
