package webservices;

import java.util.LinkedList;
import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService (name="HybridServerService", targetNamespace="http://hybridserver.dai.esei.uvigo.es/")
@SOAPBinding(style = Style.DOCUMENT)
public interface OfferedServices {
	
	@WebMethod
	String getHTML(String uuid);
	
	@WebMethod
	String[] getHTMLlist();
	
	@WebMethod
	String getXML(String uuid);
	
	@WebMethod
	String[] getXMLlist();
	
	@WebMethod
	String getXSD(String uuid);
	
	@WebMethod
	String[] getXSDlist();
	
	@WebMethod
	String getXSLT(String uuid);
	
	@WebMethod
	String[] getXSLTlist();
	
	@WebMethod
	String getAssociatedXSD(String uuid);
}
