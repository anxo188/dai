package webservices;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.jws.WebService;

import customexceptions.BadRequestException;
import customexceptions.InternalServerException;
import customexceptions.PageNotFoundException;
import utilidadesalmacenamiento.Almacenamiento;


@WebService(serviceName="HybridServerService",targetNamespace="http://hybridserver.dai.esei.uvigo.es/",endpointInterface ="webservices.OfferedServices")
public class OfferedServicesImplementation implements OfferedServices {
	private Almacenamiento storage;
	public OfferedServicesImplementation(Almacenamiento storage) {
		this.storage = storage;
	}

	@Override
	public String getHTML(String uuid) {
		
		try {
			return this.storage.getResource(uuid, "html");
		} catch (PageNotFoundException | BadRequestException | InternalServerException e) {
			return null;
		}
		
	}

	@Override
	public String[] getHTMLlist() {
		try {
			LinkedList<String> list = (LinkedList<String>) this.storage.getUUIDs("html");
			Iterator<String> it = list.iterator();
			String[] toReturn = new String[list.size()];
			int index = 0;
			while(it.hasNext()) {
				toReturn[index] = it.next();
				index++;
			}
			return toReturn;	
		} catch (BadRequestException | InternalServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getXML(String uuid) {
		try {
			return this.storage.getResource(uuid, "xml");
		} catch (PageNotFoundException | BadRequestException | InternalServerException e) {
			return null;
		}
	}

	@Override
	public String[] getXMLlist() {
		try {
			
			LinkedList<String> list = (LinkedList<String>) this.storage.getUUIDs("xml");
			Iterator<String> it = list.iterator();
			String[] toReturn = new String[list.size()];
			int index = 0;
			while(it.hasNext()) {
				toReturn[index] = it.next();
				index++;
			}
			return toReturn;
		} catch (BadRequestException | InternalServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getXSD(String uuid) {
		try {
			return this.storage.getResource(uuid, "xsd");
		} catch (PageNotFoundException | BadRequestException | InternalServerException e) {
			return null;
		}
	}

	@Override
	public String[] getXSDlist() {
		try {
			
			LinkedList<String> list = (LinkedList<String>) this.storage.getUUIDs("xsd");
			Iterator<String> it = list.iterator();
			String[] toReturn = new String[list.size()];
			int index = 0;
			while(it.hasNext()) {
				toReturn[index] = it.next();
				index++;
			}
			return toReturn;
		} catch (BadRequestException | InternalServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getXSLT(String uuid) {
		try {
			return this.storage.getResource(uuid, "xslt");
		} catch (PageNotFoundException | BadRequestException | InternalServerException e) {
			return null;
		}
	}

	@Override
	public String[] getXSLTlist() {
		try {
			LinkedList<String> list = (LinkedList<String>) this.storage.getUUIDs("xslt");
			Iterator<String> it = list.iterator();
			String[] toReturn = new String[list.size()];
			int index = 0;
			while(it.hasNext()) {
				toReturn[index] = it.next();
				index++;
			}
			return toReturn;
		} catch (BadRequestException | InternalServerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getAssociatedXSD(String uuid) {
		try {
			return this.storage.getAsociatedXsd(uuid);
		} catch (PageNotFoundException | InternalServerException |BadRequestException e) {
			return null;
		}
	}
	
}
