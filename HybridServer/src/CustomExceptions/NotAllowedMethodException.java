package customexceptions;

public class NotAllowedMethodException extends Exception {
	private static final long serialVersionUID = 1L;

	public NotAllowedMethodException() {
	}

	public NotAllowedMethodException(String message) {
		super(message);
	}

	public NotAllowedMethodException(Throwable cause) {
		super(cause);
	}

	public NotAllowedMethodException(String message, Throwable cause) {
		super(message, cause);
	}

	public NotAllowedMethodException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
