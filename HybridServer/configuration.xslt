<xsl:stylesheet
	xmlns:xsl=”http://www.w3.org/1999/XSL/Transform”version=”1.0”
>
<xsl:output method="html" indent="yes" encoding="utf-8"/>
	<xsl:template match="/">
		<html>
			<head>
				<title>Configuracion Hybrid Server</title>
			</head>
			<body>
				<h1>Configuracion Hybrid Server</h1>
				<h1>Connections</h1>
				<xsl:apply-templates select="connections"/>
				<h1>DataBase</h1>
				<xsl:apply-templates select="database"/>
				<h1>Other Servers</h1>
				<xsl:apply-templates select="servers"/>
			</body>
		</html>
	</xsl:template>
	<xsl:template match="connections">
	
	</xsl:template>
	<xsl:templatematch="database">
	
	</xsl:template>
	<xsl:template match="servers">
	
	</xsl:template>
</xsl:stylesheet>
